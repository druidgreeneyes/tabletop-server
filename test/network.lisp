(in-package #:game-server-test)

(defpackage #:game-server-test.network
  (:nicknames #:gst.network)
  (:use #:cl))

(in-package #:game-server-test.network)

(defun retry-client-connection (c)
  (invoke-restart 'retry-client-connection c))

(define-condition too-many-failures (error)
  ((text :initarg :text :reader text)
   (source :initarg :source :reader source)))

(defun start-client-thread-and-send-test-command (server-port client-port &key (try 0) (max-tries 5))
  (restart-case
    (usocket:with-client-socket (client client-stream "localhost" server-port :local-host "localhost" :local-port client-port)
      (log:info "Sending test command to client stream...")
      (gs.network::send-command client-stream :test-command "test-command arg")
      (log:info "Test command sent."))
    (retry-client-connection (c)
      (if (< try max-tries)
          (progn
            (log:warn "Connection refused. Retrying...")
            (start-client-thread-and-send-test-command server-port client-port :try (1+ try)))
          (error 'too-many-failures
                 :text (format t "Connection refused ~d times. Server probably failed to start." try)
                 :source c)))))

;; Tests start here

(parachute:define-test "send-command transmits commands correctly"
  (let* ((server-port (gs.network::find-port))
         (client-port (gs.network::find-port :exclude server-port))
         (client-thread
           (portable-threads:spawn-form
             (log:info "Starting client-thread, opening socket to localhost:~a from localhost:~a" server-port client-port)
             (handler-bind ((usocket:connection-refused-error #'retry-client-connection))
               (start-client-thread-and-send-test-command server-port client-port)))))
    (log:info "Starting server at localhost:~a" server-port)
    (usocket:with-socket-listener (server "localhost" server-port :reuse-address t)
      (log:info "Started server" server)
      (let ((result (arrow-macros:->> server
                      (usocket:socket-accept)
                      (usocket:socket-stream)
                      (read))))
        (log:info "Test command recieved" result)
        (parachute:is equal '(:test-command "test-command arg") result)))
    (portable-threads:kill-thread client-thread)))

(parachute:test 'game-server-test.network)

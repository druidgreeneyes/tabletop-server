(in-package #:game-server-test)

(defpackage #:game-server-test.client
  (:use #:cl))

(in-package #:game-server-test.client)

(5am:def-suite* client)

(5am:run!)

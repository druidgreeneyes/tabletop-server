(in-package #:game-server-test)

(defpackage #:game-server-test.file
  (:use #:cl))

(in-package #:game-server-test.file)

(parachute:define-test "read-or-empty returns empty string in case of file read error"
  (parachute:is string= "" (gs.file::read-or-empty "some-test-file-that-does-not-exist")))

(parachute:test 'game-server-test.file)

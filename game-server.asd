
;;;; game-server.asd

(asdf:defsystem #:game-server
  :author "Josh Carton"
  :license "MIT"
  :license "Specify license here"  :serial t
  :depends-on (#:sqlite
               #:usocket-server
               #:portable-threads
               #:diff-match-patch
               #:log4cl
               #:alexandria
               #:serapeum
               #:find-port
               #:arrow-macros)
  :components ((:module "src"
                :serial t
                :components ((:file "package")
                             (:file "network")
                             (:file "file")
                             (:file "server")
                             (:file "client")
                             (:file "main")))))

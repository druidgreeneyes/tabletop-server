(asdf:defsystem #:game-server-test
  :author "Josh Carton"
  :license "MIT"
  :depends-on (#:game-server
               #:fiveam
               #:prove
               #:parachute)
  :components ((:module "test"
                :serial t
                :components ((:file "package")
                             (:file "network")
                             (:file "server")
                             (:file "client")
                             (:file "file")))))

(in-package #:game-server)

(defpackage #:game-server.main
  (:use #:cl))

(in-package #:game-server.main)

(defun main (&rest args)
  (start-server))

(defun start-server (&key (host #(0 0 0 0)) (port (gs.network::find-port)))
  (log:info "Starting server...")
  (usocket:with-socket-listener (socket host port :reuse-address t)
    (log:info "Server running at ~A:~A" host port)
    (usocket::tcp-event-loop socket #'gs.server::handle-connection! :multi-threading t)))


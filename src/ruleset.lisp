(in-package #:game-server)

(defpackage #:ruleset)

(in-package #:ruleset)

#|

Defining a ruleset should be like defining a package; 
perhaps we start with a folder and a "ruleset.rl" file
(:ruleset
 (name
  (:file "file-a")
  (:file "file-b"))
 )

We look for named .rl files in the same folder ('file-a.rl', 'file-b.rl')
We read files in encounter order unless told to do otherwise by a given file:

file-b.rl
(depend-on "file-a.rl")
(rule rule1 ())
(rule rule2 ())

In which case, we go and fetch the named file(s) first, handling their dependencies in the same manner.

Things defined in a ruleset:
rule
rule is basically a function, taking some or no arguments and producing some output.
(how to encourage functional style but allow side-effects where necessary?)
(rule jump (mod)
      (roll d20 mod))

datum
datum is a constant, visible within the scope at which it is declared.
(datum standard-array ()
       (list 15 14 13 12 10 8))

board
board is a kind of datum representing a map with a specific layout.
We should provide convenient functions for A: defining boards in code
(board highway-ambush
       (from-square-grid 20 100
                         (:at-points ((10 15) (10 60) (10 90)
                                      (30 20) (30 45) (30 70))
                                     (solid-rectangle 10 15))))
and B: loading boards from files:
(board forest-crossing
       (from-file "highway-crossing.board"))
highway-crossing.txt:
####|oo|####
####|oo|####
####|oo|####
####|oo|####
----+oo+----
oooooooooooo
oooooooooooo
----+oo+----
####|oo|####
####|oo|####
####|oo|####
####|oo|####

#=tree
|=fence
      -=fence
      +=corner-fence

Shared rules:
Some things will probably need to share code with each other;
Characters and Monsters are not identical, but both probably 
have HP, movement abilities, and similar or identical ways of
interacting with eachother and/or the environment. As such,
we should provide a convenient way to do this that doesn't
require one to be a programmer to understand. The trait/interface
model seems a decent place to start.

|#

(in-package #:game-server)

(defpackage #:game-server.client
  (:nicknames #:gs.client)
  (:use #:cl))

(in-package #:game-server.client)

;;; game needs a board
;;; board is defined by the game type, but follows some general rules:
;;; board is either a positive dag
;;;    (point A <--> point B <--> point C, entities may only move between points that have connecting edges)
;;; or a negative dag
;;;    (point A <-\-> point C, entities may move freely between points only if those points don't have connecting edges)
;;; in either case, edges define/modify movement between their respective points. Using Chutes and Ladders as an example:
;;;    (point C -Forced-> point A [representing a chute]
;;;     point A --> point B
;;;     point B --> point C
;;;     point B -Optional-> point D [representing a ladder])

;;; all of the work is done client-site; server just maintains a central event log.
;;; client A does some stuff and passes a time-stamped result to the server
;;;      (move player-a new-location)
;;; server appends this to the event log and sense the two most recent events out to all clients
;;; whenever a client recieves a pair of events, it checks to make sure its most recent event matches
;;; the second-most-recent of the pair. If not, it asks the server for a longer backlog, and continues asking until it
;;; sees events it recognizes, at which point it rebuilts its own local event log from the provided server data.

;;; server and clients each maintain local copies of game information (board, rules, state).
;;; Connecting clients either start a new game (for which they supply a game type or rules file)
;;; or supply the name of a saved game. In the case of conflicts between client saves server saves,
;;; the server always takes priority.

;;; upon loading or starting a game session, the server generates a random unique session id and hands it out to clients,
;;; which provide the session id as a prefix to any command or data passed to the server in regard to the session.

;;; this enables the game server to maintain several sessions at once, and means that without the sesson id, the server
;;; cannot know against which session to make changes it is passed.

;;; Commands to the server:
;;;   :request-log -> integer n -> returns the n most recent entries in the server's (canonical) event log
;;;   :request-ruleset -> string ruleset-hash -> returns the server's version of the hashed ruleset
;;;   :ruleset -> string ruleset-hash -> ruleset-code ruleset -> no return value, transfers a copy of the ruleset to the server
;;;   :ruleset-patch -> string ruleset-hash -> patch patch -> no return value, applies the patch to the identified ruleset
;;;
;;;   timestamp (no command) -> string event -> no return value, inserts the event into the server's event log with the given timestamp
;;;      (should probaby work out a way to resolve timestamp inaccuracies between clients. Maybe all clients poll a known time server?)

;;; Commands from the server:
;;;   :request-ruleset -> String ruleset-hash -> should return the client's version of the hashed ruleset
;;;   :use-ruleset -> string ruleset-hash -> no return value, should cause us to load the supplied ruleset locally or request it from the server if we don't have it.

(defparameter *db-path* "db")

(defun push-ruleset! (server-stream ruleset-hash)
  (let ((ruleset (load-ruleset ruleset-hash)))
    (send-to-server! server-stream :ruleset ruleset-hash ruleset)))

(defun request-ruleset! (server-stream ruleset-hash)
  (send-to-server! server-stream :request-ruleset ruleset-hash))

(defun get-ruleset-path (ruleset-hash)
  (first (directory (pathname (format nil "~A/rulesets/~A" *db-path* ruleset-hash)))))

(defun save-ruleset! (ruleset-hash ruleset)
  (when (and ruleset-hash ruleset)
    (arrow-macros:->> ruleset-hash
      (get-ruleset-path)
      (alexandria:write-string-into-file ruleset))))

(defun load-ruleset (ruleset-hash)
  (arrow-macros:some->> ruleset-hash
    (get-ruleset-path)
    (gs.file::read-or-empty)))

(defun patch-ruleset (ruleset-hash patch)
  (arrow-macros:some->> ruleset-hash
    (load-ruleset)
    (dmp:apply-patch patch)
    #'(lambda (p) (list (sxhash p) p))
    (apply save-ruleset!)))

(defun dispatch-input! (server-stream)
  (let ((input (gs.network::read-from-stream server-stream)))
    (case (first input)
      (:request-ruleset (push-ruleset! (usocket:socket-stream server-socket) (second input)))
      (:ruleset (apply #'save-ruleset! (rest input)))
      (:ruleset-patch (apply #'patch-ruleset! (rest input)))
      (:use-ruleset (apply #'use-ruleset! (rest input)))
      (:request-client-id (send-client-id! server-socket))
      (otherwise (log-event! event-log input)))))

(defun send-handshake! (server-socket game-id)
  (let ((ruleset-hash (get-ruleset-hash game-id)))
    (gs.network::write-to-stream server-stream game-id ruleset-hash)))

(defun connect-to-game (game-id host &key (port 10600))
  (let ((socket (usocket:socket-connect host port :element-type 'character)))
    (send-handshake! socket)))

(defun send-to-server! (server-stream command-keyword &rest body)
  (gs.network::send-command server-stream command-keyword body))

(in-package #:game-server)

(defpackage #:game-server.network
  (:nicknames #:gs.network)
  (:use #:cl))

(in-package #:game-server.network)

(defun write-to-stream (stream content)
  (write content :stream stream :readably t))

(defun send-command (stream command &rest params)
  (write-to-stream stream (cons command params)))

(defun broadcast (content streams)
  (loop for stream in streams
        do (write-to-stream content streams)))

(defun port-open-p (port)
  "Determine if the port is open."
  (handler-case
      (let ((socket (usocket:socket-listen "127.0.0.1" port
                                           :reuse-address t)))
        (usocket:socket-close socket))
    (usocket:address-in-use-error (condition)
      (declare (ignore condition))
      nil)))

(defun find-port (&key (min 40000) (max 65500) (exclude nil))
  (let ((exclude-list (if (listp exclude) exclude (list exclude)))
        (bound (- max min)))
    (loop for port = (+ min (random bound))
          when (and
                (not (member port exclude-list))
                (port-open-p port))
          return port)))

-(in-package #:game-server)

(defpackage #:game-server.file
  (:nicknames #:gs.file)
  (:use #:cl))

(in-package #:game-server.file)

(defun read-or-empty (pathname)
  (handler-case
      (alexandria:read-file-into-string pathname)
    (condition (c)
      (log:warn "Unable to read file at ~a!" pathname c)
      "")))
